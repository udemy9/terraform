variable "aws-vpc-cidr" {
    type = string
    description = "CIDR block to be used in the VPC"
    default = "10.0.0.0/16"
}

resource "aws_vpc" "terraform_vpc" {
  cidr_block = var.aws-vpc-cidr
  instance_tenancy = "default"
  tags = {
    "Name" = "Terraform VPC"
    "Terraform" = "True"
  }
}