resource "aws_security_group" "terraform-alb-security-group" {
    vpc_id = aws_vpc.terraform_vpc.id
    name = "Terraform ALB Security Group"
    egress {
        from_port = 0
        to_port = 0
        protocol = "-1"
        cidr_blocks = [
            "0.0.0.0/0"
        ]
    }
    ingress {
        from_port = 443
        to_port = 443
        protocol = "tcp"
        cidr_blocks = [
            "0.0.0.0/0"
        ]
    }
    tags = {
      "Name" = "Terraform ALB Security Group"
      "Terraform" = "True"
    }
}

resource "aws_alb" "terraform-alb" {
  name = "terraform-app-load-balancer"
  internal = false
  load_balancer_type = "application"
  enable_deletion_protection = false
  security_groups = [
      aws_security_group.terraform-alb-security-group.id
  ]
  subnets = [
      aws_subnet.terraform-public-1a.id,
      aws_subnet.terraform-public-1b.id
  ]
  tags = {
    "Name" = "Terraform Application Load Balancer",
    "Terraform" = "True"
  }
}