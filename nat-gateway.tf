resource "aws_eip" "terraform-nat1" {
}

resource "aws_eip" "terraform-nat2" {
}

resource "aws_nat_gateway" "terraform-nat-gateway-1a" {
  allocation_id = aws_eip.terraform-nat1.id
  subnet_id = aws_subnet.terraform-public-1a.id
  tags = {
    "Name" = "Terraform NAT Gateway 1a"
    "Terraform" = "True"
  }
}

resource "aws_nat_gateway" "terraform-nat-gateway-1b" {
  allocation_id = aws_eip.terraform-nat2.id
  subnet_id = aws_subnet.terraform-public-1b.id
  tags = {
    "Name" = "Terraform NAT Gateway 1b"
    "Terraform" = "True"
  }
}