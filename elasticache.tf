resource "aws_security_group" "terraform-elasticache-sg" {
  name = "Terraform Elasticache Security Group"
  vpc_id = aws_vpc.terraform_vpc.id
  egress {
    from_port = 0
    to_port = 0
    protocol = "-1"
    cidr_blocks = [
      "0.0.0.0/0"
    ]
  }
  ingress {
    from_port = 6379
    to_port = 6379
    protocol = "tcp"
    security_groups = [
      aws_security_group.terraform-asg-sg.id
    ]
  }
  tags = {
    "Name" = "Terraform Elasticache Security Group"
    "Terraform" = "True"
  }
}

resource "aws_elasticache_subnet_group" "terraform-elasticache-subnet" {
  name = "Terraform-Elasticache-Subnet-Group"
  subnet_ids = [
    aws_subnet.terraform-private-1a.id,
    aws_subnet.terraform-private-1b.id
    ]
}

resource "aws_elasticache_replication_group" "terraform-elasticache" {
  automatic_failover_enabled = "true"
  replication_group_id = "terraform-replication-group"
  replication_group_description = "terraform-elasticache-group"
  node_type = "cache.t2.micro"
  number_cache_clusters = 2
  engine_version = "5.0.4"
  parameter_group_name = "default.redis5.0"
  port = 6379
  subnet_group_name = aws_elasticache_subnet_group.terraform-elasticache-subnet.name
  security_group_ids = [
    aws_security_group.terraform-elasticache-sg.id
  ]
  tags = {
    "Name" = "Terraform Elasticache Replication Group"
    "Terraform" = "True"
  }
}