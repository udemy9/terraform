resource "aws_security_group" "terraform-bastion-security-group" {
  vpc_id = aws_vpc.terraform_vpc.id
  name = "Terraform Bastion Host Security group"
  egress {
      from_port = 0
      to_port = 0
      protocol = "-1"
      cidr_blocks = [
          "0.0.0.0/0"
      ]
  }
  ingress {
      from_port = 22
      to_port = 22
      protocol = "tcp"
      cidr_blocks = [
          "0.0.0.0/0"
      ]
  }
  tags = {
    "Name" = "Terraform Security Group for bastion host"
    "Terraform" = "True"
  }
}

data "template_file" "db-import-template" {
  template = file("db-user-data.tpl")
  vars = {
    db_host = aws_db_instance.terraform-db.address
    db_username = aws_db_instance.terraform-db.username
    db_password = var.db-master-password
    db_name = aws_db_instance.terraform-db.name
  }
}

resource "aws_instance" "terraform-bastion-host-1a" {
  ami = "ami-06fd8a495a537da8b"
  instance_type = "t2.micro"
  key_name = aws_key_pair.terraform-ssh-key.key_name
  associate_public_ip_address = true
  vpc_security_group_ids = [
      aws_security_group.terraform-bastion-security-group.id
  ]
  subnet_id = aws_subnet.terraform-public-1a.id
  iam_instance_profile = aws_iam_instance_profile.bastion-host-profile.id
  user_data = data.template_file.db-import-template.rendered
  tags = {
    "Name" = "Terraform Bastion Host 1a"
    "Terraform" = "True"
  }
}

resource "aws_instance" "terraform-bastion-host-1b" {
  ami = "ami-06fd8a495a537da8b"
  instance_type = "t2.micro"
  key_name = aws_key_pair.terraform-ssh-key.key_name
  associate_public_ip_address = true
  vpc_security_group_ids = [
      aws_security_group.terraform-bastion-security-group.id
  ]
  subnet_id = aws_subnet.terraform-public-1b.id
  tags = {
    "Name" = "Terraform Bastion Host 1b"
    "Terraform" = "True"
  }
}