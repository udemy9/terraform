resource "aws_internet_gateway" "terraform-internet-gateway" {
  vpc_id = aws_vpc.terraform_vpc.id
  tags = {
    "Name" = "Terraform Internet Gateway"
    "Terraform" = "True"
  }
}