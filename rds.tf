resource "aws_security_group" "terraform-db-sg" {
    vpc_id = aws_vpc.terraform_vpc.id
    name = "terraform rds security group"
    egress {
        from_port = 0
        to_port = 0
        protocol = "-1"
        cidr_blocks = [
            "0.0.0.0/0"
        ]
    }
    ingress {
        from_port = 3306
        to_port = 3306
        protocol = "tcp"
        security_groups = [
            aws_security_group.terraform-asg-sg.id,
            aws_security_group.terraform-bastion-security-group.id,
        ]
    }
    tags = {
      "key" = "Terraform RDS Security Group"
      "Terraform" = "True"
    }
}

resource "aws_db_subnet_group" "terraform-db-subnet" {
  name = "terraform database subnet group"
  subnet_ids = [
      aws_subnet.terraform-private-1a.id,
      aws_subnet.terraform-private-1b.id
  ]
  tags = {
    "Name" = "Terraform DB Subnet group"
    "Terraform" = "True"
  }
}

resource "aws_db_instance" "terraform-db" {
  allocated_storage = 10
  storage_type = "gp2"
  engine = "mysql"
  engine_version = "5.6"
  multi_az = true
  instance_class = "db.t2.micro"
  name = "terraform"
  username = "admin"
  password = var.db-master-password
  identifier = "terraform-database"
  skip_final_snapshot = true
  backup_retention_period = 7
  port = 3306
  storage_encrypted = false
  db_subnet_group_name = aws_db_subnet_group.terraform-db-subnet.name
  vpc_security_group_ids = [
      aws_security_group.terraform-db-sg.id
  ]
  tags = {
    "Name" = "Terraform Database"
    "Terraform" = "True"
  }
}