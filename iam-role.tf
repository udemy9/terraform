resource "aws_iam_role" "bastion-host-role" {
  name = "ec2-s3-access-role"
  assume_role_policy = <<EOF
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Sid": "",
      "Effect": "Allow",
      "Principal": {
        "Service": "ec2.amazonaws.com"
      },
      "Action": "sts:AssumeRole"
    }
  ]
}
EOF
}

resource "aws_iam_role_policy_attachment" "bastion-host-policy" {
  policy_arn = "arn:aws:iam::aws:policy/AmazonS3FullAccess"
  role = aws_iam_role.bastion-host-role.id
}

resource "aws_iam_instance_profile" "bastion-host-profile" {
  name = "bastion-host-s3-access-profile"
  role = aws_iam_role.bastion-host-role.name
}