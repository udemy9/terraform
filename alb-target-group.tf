resource "aws_lb_target_group" "terraform-front-end-tg" {
    port = 80
    protocol = "HTTP"
    name = "terraform-front-end-target-group"
    vpc_id = aws_vpc.terraform_vpc.id
    stickiness {
      type = "lb_cookie"
      enabled = true
    }
    health_check {
      protocol = "HTTP"
      path = "/healthy.html"
      healthy_threshold = 2
      unhealthy_threshold = 2
      timeout = 5
      interval = 10
    }
    tags = {
      "Name" = "Terraform Front End Target Group"
      "Terraform" = "True"
    }
}

resource "aws_lb_target_group" "terraform-back-end-tg" {
    port = 80
    protocol = "HTTP"
    name = "terraform-back-end-target-group"
    vpc_id = aws_vpc.terraform_vpc.id
    stickiness {
      type = "lb_cookie"
      enabled = true
    }
    health_check {
      protocol = "HTTP"
      path = "/healthy.html"
      healthy_threshold = 2
      unhealthy_threshold = 2
      timeout = 5
      interval = 10
    }
    tags = {
      "Name" = "Terraform Back End Target Group"
      "Terraform" = "True"
    }
}