resource "aws_security_group" "terraform-efs-sg" {
  name = "Terraform EFS Security Group"
  vpc_id = aws_vpc.terraform_vpc.id
  egress {
    from_port = 0
    to_port = 0
    protocol = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }
  ingress {
    from_port = 2049
    to_port = 2049
    protocol = "tcp"
    security_groups = [
      aws_security_group.terraform-asg-sg.id
    ]
  }
  tags = {
    "Name" = "EFS Security Group"
    "Terraform" = "True"
  }
}

resource "aws_efs_file_system" "terraform-efs" {
  creation_token = "terraform-elastic-file-system" 
  performance_mode = "generalPurpose"
  throughput_mode = "bursting"
  tags = {
    "Name" = "Terraform Elastic File System"
    "Terraform" = "True"
  }
}

resource "aws_efs_mount_target" "terraform-mount-private-1a" {
  file_system_id = aws_efs_file_system.terraform-efs.id
  subnet_id = aws_subnet.terraform-private-1a.id
  security_groups = [
    aws_security_group.terraform-efs-sg.id
  ]
}
resource "aws_efs_mount_target" "terraform-mount-private-1b" {
  file_system_id = aws_efs_file_system.terraform-efs.id
  subnet_id = aws_subnet.terraform-private-1b.id
  security_groups = [
    aws_security_group.terraform-efs-sg.id
  ]
}