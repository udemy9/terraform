resource "aws_route_table" "terraform-private-route-table-1a" {
  vpc_id = aws_vpc.terraform_vpc.id
  route {
      cidr_block = "0.0.0.0/0"
      gateway_id = aws_nat_gateway.terraform-nat-gateway-1a.id
  }
  tags = {
    "Name" = "Terraform private Route table 1a"
    "Terraform" = "True"
  }
}

resource "aws_route_table" "terraform-private-route-table-1b" {
  vpc_id = aws_vpc.terraform_vpc.id
  route {
      cidr_block = "0.0.0.0/0"
      gateway_id = aws_nat_gateway.terraform-nat-gateway-1b.id
  }
  tags = {
    "Name" = "Terraform private Route table 1b"
    "Terraform" = "True"
  }
}

resource "aws_route_table_association" "terraform-private-1a-association" {
  subnet_id = aws_subnet.terraform-private-1a.id
  route_table_id = aws_route_table.terraform-private-route-table-1a.id
}

resource "aws_route_table_association" "terraform-private-1b-association" {
  subnet_id = aws_subnet.terraform-private-1b.id
  route_table_id = aws_route_table.terraform-private-route-table-1b.id
}