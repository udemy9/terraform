resource "aws_security_group" "terraform-asg-sg" {
  vpc_id = aws_vpc.terraform_vpc.id
  name = "Terraform ASG Security Group"
  egress {
      from_port = 0
      to_port = 0
      protocol= "-1"
      cidr_blocks = [
          "0.0.0.0/0"
      ]
  }
  ingress {
      from_port = 80
      to_port = 80
      protocol = "tcp"
      security_groups = [
          aws_security_group.terraform-alb-security-group.id
      ]
  }
  ingress {
      from_port = 22
      to_port = 22
      protocol = "tcp"
      security_groups = [
          aws_security_group.terraform-alb-security-group.id
      ]
  }
  tags = {
    "Name" = "Terraform ASG Security Group"
    "Terraform" = "True"
  }
}

data "template_file" "userdata-template" {
  template = file("user-data.tpl")
  vars = {
      db_host = aws_db_instance.terraform-db.address
      db_username = aws_db_instance.terraform-db.username
      db_password = var.db-master-password
      db_name = aws_db_instance.terraform-db.name
      cache_host = aws_elasticache_replication_group.terraform-elasticache.primary_endpoint_address
      efs-endpoint = aws_efs_file_system.terraform-efs.dns_name
  }
}

resource "aws_launch_configuration" "terraform-launch-config" {
  name_prefix = "Terraform Launch Configuration"
  image_id = "ami-05a366f8cb3ece0ac"
  instance_type = "t2.micro"
  security_groups = [
      aws_security_group.terraform-asg-sg.id
  ]
  key_name = aws_key_pair.terraform-ssh-key.key_name
  user_data = data.template_file.userdata-template.rendered
  lifecycle {
      create_before_destroy = true
  }
}

resource "aws_autoscaling_group" "terraform-front-end" {
  name = "Terraform Front End ASG"
  launch_configuration = aws_launch_configuration.terraform-launch-config.name
  health_check_type = "ELB"
  min_size = 0
  max_size = 0
  desired_capacity = 0
  vpc_zone_identifier = [
      aws_subnet.terraform-private-1a.id,
      aws_subnet.terraform-private-1b.id
  ]
  target_group_arns = [
      aws_lb_target_group.terraform-front-end-tg.id
  ]
  lifecycle {
      create_before_destroy = true
  }
  tag {
      key = "Name"
      value = "Terraform Front End ASG"
      propagate_at_launch = true
  }
}

resource "aws_autoscaling_group" "terraform-back-end" {
  name = "Terraform Back End ASG"
  launch_configuration = aws_launch_configuration.terraform-launch-config.name
  health_check_type = "ELB"
  min_size = 0
  max_size = 0
  desired_capacity = 0
  vpc_zone_identifier = [
      aws_subnet.terraform-private-1a.id,
      aws_subnet.terraform-private-1b.id
  ]
  target_group_arns = [
      aws_lb_target_group.terraform-back-end-tg.id
  ]
  lifecycle {
      create_before_destroy = true
  }
  tag {
      key = "Name"
      value = "Terraform Back End ASG"
      propagate_at_launch = true
  }
}