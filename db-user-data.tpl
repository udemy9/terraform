#!/bin/bash

sudo apt-get update -y
sudo apt-get install -y httpd24 php70 mysql56-server php70-mysqlnd

cd ~
curl "https://awscli.amazonaws.com/awscli-exe-linux-x86_64.zip" -o "awscliv2.zip"
unzip awscliv2.zip
sudo ./aws/install

aws s3 cp s3://terraform-tutorial/database.zip .
unzip database.zip

cd database
mysql -h ${db_host} -u ${db_username} -p${db_password} magento < magento.sql
rm -rf ~/database.zip ~/database

mysql -h ${db_host} -u ${db_username} -p${db_password} -D ${db_name} --execute "UPDATE core_config_data SET value = 'https://www.rinnegan.io' WHERE path LIKE 'web/unsecure/base_url'; UPDATE core_config_data SET value = 'https://www.rinnegan.io' WHERE path LIKE 'web/secure/base_url';"