resource "aws_subnet" "terraform-public-1a" {
  vpc_id = aws_vpc.terraform_vpc.id
  cidr_block = "10.0.1.0/24"
  availability_zone = "eu-west-1a"
  map_public_ip_on_launch = true
  tags = {
    "Name" = "Terraform Public Subnet 1a"
    "Terraform" = "True"
  }
}

resource "aws_subnet" "terraform-public-1b" {
  vpc_id = aws_vpc.terraform_vpc.id
  cidr_block = "10.0.2.0/24"
  availability_zone = "eu-west-1b"
  map_public_ip_on_launch = true
  tags = {
    "Name" = "Terraform Public Subnet 1b"
    "Terraform" = "True"
  }
}

resource "aws_subnet" "terraform-private-1a" {
  vpc_id = aws_vpc.terraform_vpc.id
  cidr_block = "10.0.3.0/24"
  availability_zone = "eu-west-1a"
  map_public_ip_on_launch = false
  tags = {
    "Name" = "Terraform Private Subnet 1a"
    "Terraform" = "True"
  }
}

resource "aws_subnet" "terraform-private-1b" {
  vpc_id = aws_vpc.terraform_vpc.id
  cidr_block = "10.0.4.0/24"
  availability_zone = "eu-west-1b"
  map_public_ip_on_launch = false
  tags = {
    "Name" = "Terraform Private Subnet 1b"
    "Terraform" = "True"
  }
}