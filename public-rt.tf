resource "aws_route_table" "terraform-public" {
  vpc_id = aws_vpc.terraform_vpc.id
  route {
      cidr_block = "0.0.0.0/0"
      gateway_id = aws_internet_gateway.terraform-internet-gateway.id
  }
  tags = {
    "Name" = "Terraform Public Route Table"
    "Terraform" = "True"
  }
}

resource "aws_route_table_association" "terraform-public-1a-association" {
  subnet_id = aws_subnet.terraform-public-1a.id
  route_table_id = aws_route_table.terraform-public.id
}

resource "aws_route_table_association" "terraform-public-1b-association" {
  subnet_id = aws_subnet.terraform-public-1b.id
  route_table_id = aws_route_table.terraform-public.id
}